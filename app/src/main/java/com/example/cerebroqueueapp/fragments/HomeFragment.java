package com.example.cerebroqueueapp.fragments;


import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.cerebroqueueapp.R;
import com.example.cerebroqueueapp.adapters.SectionsPagerAdapter;
import com.google.android.material.tabs.TabLayout;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    private View view;
    private Context context;

    private ViewPager view_pager;
    private SectionsPagerAdapter viewPagerAdapter;
    private TabLayout tab_layout;
    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);
        context = getContext();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        initializeUI();
    }

    private void initializeUI(){
        view_pager = (ViewPager) view.findViewById(R.id.view_pager);
        tab_layout = (TabLayout) view.findViewById(R.id.tab_layout);
        setupViewPager(view_pager);

        tab_layout.setupWithViewPager(view_pager);
    }

    private void setupViewPager(ViewPager viewPager) {
        viewPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());
        viewPagerAdapter.addFragment(CashierFragment.newInstance(), "Payment");    // index 0
        viewPagerAdapter.addFragment(BillingFragment.newInstance(), "Billing");   // index 1
        viewPagerAdapter.addFragment(CustomerServiceFragment.newInstance(), "Customer Service");    // index 2
        viewPager.setAdapter(viewPagerAdapter);
    }

}
