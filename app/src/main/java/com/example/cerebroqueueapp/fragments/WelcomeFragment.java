package com.example.cerebroqueueapp.fragments;


import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.cerebroqueueapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class WelcomeFragment extends Fragment {
    private View view;
    private Context context;
    private Button tab_payment, tab_billing, tab_customer_service;

    public WelcomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_welcome, container, false);
        context = getContext();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        initializeUI();
    }

    private void initializeUI() {
        tab_payment = view.findViewById(R.id.tab_payment);
        tab_billing = view.findViewById(R.id.tab_billing);
        tab_customer_service = view.findViewById(R.id.tab_customer_service);
        tab_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTicketDialog();
            }
        });
        tab_billing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTicketDialog();
            }
        });
        tab_billing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTicketDialog();
            }
        });
    }
    int progress = 0;
    ProgressBar indeterminateBar;
    AlertDialog dialog;
    private void showTicketDialog() {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
        View view = getLayoutInflater().inflate(R.layout.layout_coupon_number, null);
        TextView tv_number = view.findViewById(R.id.tv_number);
        indeterminateBar = view.findViewById(R.id.indeterminateBar);

        mBuilder.setView(view);
        dialog = mBuilder.create();
        dialog.show();

        setProgressValue(progress);
        //dialog.dismiss();
    }
    private void setProgressValue(final int progress) {
        indeterminateBar.setProgress(progress);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                setProgressValue(progress + 20);
            }
        });
        thread.start();
    }
}
