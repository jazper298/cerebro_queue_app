package com.example.cerebroqueueapp.fragments;


import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.cerebroqueueapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CashierFragment extends Fragment {
    private View view;
    private Context context;

    public CashierFragment() {
        // Required empty public constructor
    }

    public static CashierFragment newInstance() {
        CashierFragment fragment = new CashierFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_cashier, container, false);
        return view;
    }

}
